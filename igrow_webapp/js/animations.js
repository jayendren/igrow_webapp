/*! code resuse:
 * Ref: https://stackoverflow.com/questions/21360519/jquery-fadein-on-page-load
 * fade in main-content
*/
$(function() {
    $('div.hidden').fadeIn(950).removeClass('hidden');
});
/*! code reuse:
 * Animate dropdown menus
 * Ref: https://stackoverflow.com/questions/12115833/adding-a-slide-effect-to-bootstrap-dropdown	
 */	
$(function(){
    // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // ADD SLIDEUP ANIMATION TO DROPDOWN //
    $('.dropdown').on('hide.bs.dropdown', function(e){
        e.preventDefault();
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(400, function(){
            //On Complete, we reset all active dropdown classes and attributes
            //This fixes the visual bug associated with the open class being removed too fast
            $('.dropdown').removeClass('open');
            $('.dropdown').find('.dropdown-toggle').attr('aria-expanded','false');
        });
    });
});
/*! code reuse:
 * Trigger dropdown menus
 * Ref: https://stackoverflow.com/questions/22842903/how-to-open-bootstrap-dropdown-programmatically
 */
$('.trigger_button').click(function(e) {
	e.stopPropagation();
	$('.dropdown-toggle').dropdown('toggle');
});

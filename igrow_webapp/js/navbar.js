/*! code reuse:
 * Show/hide Left navbar
 * Ref: https://codepen.io/nirmalkc/pen/JKjmVg?editors=0010 
*/
$('.btn-expand-collapse').click(function(e) {
	$('.navbar-primary').toggleClass('expanded');
});
/*! code reuse:
 * Scroll to top button
 * Ref: http://www.webtipblog.com/adding-scroll-top-button-website/
*/
$(function() { 
	$(document).on( 'scroll', function(){
 
		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
	});
 
	$('.scroll-top-wrapper').on('click', scrollToTop);
}); 
function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}

In this assessment, using the BootStrap and jQuery that you have learned in this module, you are to develop a web site prototype for a complex web app. 

Your application should have the following features:

A website with several pages (e.g. 4 to 10). 

1. Home
2. Grow
3. Track Crop
4. Tutorials
5. Help
6. Contact
7. About - Company
8. About - Product


Your submission should include a zipped website containing a selection of html pages and a sensible folder structure.

> place images in img/
> place html (except index) in pages/

Each page should contain a relevant doctype, be well commented, well formatted and free from JavaScript errors. 

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Index</title>
  <meta charset="utf-8">  
  

Your comments need not be exhaustive but should describe what each of the main sections in the pages do and approximately how they work.

> e.g format:
  	<!-- Navbars 			
  		code reuse  
  		Ref: https://codepen.io/nirmalkc/pen/JKjmVg?editors=0010   			
  		Create a top and a collapsible left navbar
  		Requires css/custom.css and js/custom.js
  	-->

Based on Bootstrap and jQuery, your site should contain and use the appropriate bootstrap css and js. i.e. sites intended for mobile use should be fully responsive.

> <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Responsive -->
> make use of container-fluid

A consistent navigation system (e.g.: nav bar). 

The user should never feel lost or unable to find their way to any of the main sections of your site, regardless of where they enter the site or navigate around it.

> opted for navbar-top and navbar-left
> navbar-left is expandable/collapsable


Include a selection of Bootstrap components. See http://getbootstrap.com/components/ 

> min 2

> 1. Navbar
> 2. Glyphicons for navbar
> 3. Carousel

Include a transition effect (or some other animated jQuery effect). See http://getbootstrap.com/javascript

> 1. Modal for contact page - https://getbootstrap.com/docs/3.3/javascript/#modals
> 2. Animated Dropdown menu

You should include some animated transition effect(s) on each page of your site. CSS hover effects are not sufficient – aim for some Bootstrap / jQuery here.

> fade in main content using igrow.css and animations.js
> scroll to top in nabar-left using navbar.css and navbar.js

Include your own CSS and use some of your own classes and id's. 
At least some elements on your site should include your own styling. Use best practices when deciding where to include these.

> igrow.css
> navbar.css
> wizard.css


> https://stackoverflow.com/questions/2511048/how-do-you-refer-to-more-than-one-css-file-in-html

Communicate with at least one Bootstrap plugin using Data Attributes i.e. data toggle / data target.

> navbar 
		        	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
> modal
					<a href="" class="fa fa-envelope-square" data-toggle="modal" data-target="#modalContactForm"></a>

Use best practices when thinking about how your pages will load, i.e. JS, etc.

> execute js after document.ready events (jquery)

Please note: all code should be fully-commented and referenced appropriately, with files placed in a sensible hierarchy. 
Your work must be zipped in a single folder for submission. 
If you use reused code, please use inline comments to state its origin.

//code reuse

//Ref: http:…. Or citation of book used, etc.

> done
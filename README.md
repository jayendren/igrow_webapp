### Table of Contents

- [About](#about)
- [Requirements](#requirements)
  * [Bootstrap](#bootstrap)
  * [Web Fonts](#web-fonts)
  * [Web Server](#web-server)
- [Directory layout](#directory-layout)
- [Deployment instructions](#deployment-instructions)
  * [webserver](#webserver)
    + [httpd/apache](#httpd-apache)
    + [nginx](#nginx)
- [Growing](#growing)
- [Tracking](#tracking)
- [Tutorials](#tutorials)
- [Help](#help)
- [Contact](#contact)
- [About Company](#about-company)
- [About Product](#about-product)
- [Account management](#account-management)

### About


iGrow is a Web Application with the intention of educating and connecting potential users, in the farming value chain, who live in smart cities and enable them to reach their goals on how to grow and/or monetize food with limited resources and time.
	            

![](demo/demo-home.jpg)

### Requirements

#### Bootstrap

* Uses an offline version of bootstrap - http://getbootstrap.com/docs/3.3/getting-started/#download

#### Web Fonts

* Uses an offline version of fontawesome - https://fontawesome.com/get-started
* Uses an online version of OpenSans - https://fonts.googleapis.com/css?family=Open+Sans

#### Web Server

* nginx or httpd web server

### Directory layout
```
igrow_webapp # www_root
├── css
│   ├── bootstrap-theme.css
│   ├── bootstrap-theme.min.css
│   ├── bootstrap.css
│   ├── bootstrap.min.css
│   ├── calendar.css
│   ├── font-awesome.min.css
│   ├── igrow.css
│   ├── navbar.css
│   └── wizard.css
├── favicon.ico
├── fonts
│   ├── FontAwesome.otf
│   ├── fontawesome-webfont.eot
│   ├── fontawesome-webfont.svg
│   ├── fontawesome-webfont.ttf
│   ├── fontawesome-webfont.woff
│   ├── fontawesome-webfont.woff2
│   ├── glyphicons-halflings-regular.eot
│   ├── glyphicons-halflings-regular.svg
│   ├── glyphicons-halflings-regular.ttf
│   ├── glyphicons-halflings-regular.woff
│   └── glyphicons-halflings-regular.woff2
├── img
│   ├── demo-user-men-10.jpg
│   ├── demo-user-men-23.jpg
│   ├── demo-user-men-27.jpg
│   ├── demo-user-men-61.jpg
│   ├── demo-user-men-66.jpg
│   ├── demo-user-women-31.jpg
│   ├── demo-user-women-47.jpg
│   ├── demo-user-women-54.jpg
│   ├── demo-user-women-93.jpg
│   ├── igrow-hydro-strawberries.jpg
│   ├── igrow-leafy-veges.jpg
│   ├── igrow-smart-city-community-farm.jpg
│   ├── igrow-smart-city-gardening-flats.jpg
│   ├── igrow-smart-city-urban-farm.jpg
│   ├── igrow_background.jpg
│   └── radical-techs-logo.jpg
├── index.html
├── js
│   ├── animations.js
│   ├── bootstrap.js
│   ├── bootstrap.min.js
│   ├── calendar.js
│   ├── jquery-3.3.1.min.js
│   ├── navbar.js
│   └── wizard.js
└── pages
    ├── company.html
    ├── contact.html
    ├── grow.html
    ├── help.html
    ├── product.html
    ├── track.html
    └── tutorials.html

5 directories, 52 files
```
### Deployment instructions

* the igrow_webapp (www root directory) needs to be placed in the web root of the webserver


#### webserver

##### httpd/apache

* place the igrow_webapp in the defined DocumentRoot directive - https://httpd.apache.org/docs/2.4/mod/core.html#documentroot
* e.g: ```/var/www/sites``` in unix based systems, or ```c:\www\sites``` in windows based systems

##### nginx

* place the igrow_webapp in the defined root directive - http://nginx.org/en/docs/http/ngx_http_core_module.html#root
* e.g: ```/var/www/sites``` in unix based systems, or ```c:\www\sites``` in windows based systems 


### Growing

* Access the Grow Wizard by clicking on the Grow icon on the left navbar (tap on the ">" to reveal icon names)
![](demo/demo-expand-navbar.jpg)
* Select your Region
![](demo/demo-grow-step-1.jpg)
* Select your Crop
![](demo/demo-grow-step-2.jpg)
* Choose to buy equipment, seeds and/or fertilizers
![](demo/demo-grow-step-3.jpg)
* You may choose to correct any steps by tapping on the Step Number (e.g (1) - Region) and finally Choose Finish
![](demo/demo-grow-step-4.jpg)
        
### Tracking

* Access the Tracking Calendar by clicking on the Track icon on the left navbar (tap on the ">" to reveal icon names)
![](demo/demo-track.jpg)
* View Events details by tapping on any of the events in the calendar
![](demo/demo-track-event.jpg)

### Tutorials

* Access the Tutorials by clicking on the Tutorial icon on the left navbar (tap on the ">" to reveal icon names)
![](demo/demo-tutorials.jpg)

### Help

* Access the Help by clicking on the Help icon on the left navbar (tap on the ">" to reveal icon names)
* Forum and Order tracking has not been implemented
![](demo/demo-help.jpg)

### Contact

* Access the Contact page by clicking on the Contact icon on the left navbar (tap on the ">" to reveal icon names)
![](demo/demo-contact.jpg)

### About Company

* Access the Company page by clicking on the About icon on the right navbar and then tapping on Company
![](demo/demo-about.jpg)
![](demo/demo-company.jpg)

### About Product

* Access the Product page by clicking on the About icon on the right navbar and then tapping on Product
![](demo/demo-about.jpg)
* Tap on any of the Key Features to view more information
![](demo/demo-product.jpg)

### Account management

* This module was is out of scope and displays nothing
![](demo/demo-myaccount.jpg)